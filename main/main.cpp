// A simple program that computes the square root of a number
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>

#include "../src/someFile.h"
#include "../src/SimpleRpcServer.h"

#include <SimpleAmqpClient/SimpleAmqpClient.h>

using namespace std;

using namespace AmqpClient;

#include <msgpack.hpp>
//#include <string>
//#include <iostream>
#include <sstream>

int main() 
{
  cout << "hello World." << endl;

  string hostname("myrabbitmqhostname");
  string rpc_queue("rpc_queue");
  int port=5672;
  string username("guest");
  string password("guest");

  //establish connection
  int frame_max(131072); // 131072 = 128kb. Client gets disconnected if it sends a larger message?
  string vhost("/");
  
  //Channel::OpenOpts opts = connected_test::GetTestOpenOpts();
  //opts.frame_max = 4096;
  //Channel::ptr_t channel = Channel::Open(opts);

  Channel::ptr_t channel = Channel::Create(hostname, port, username, password, vhost, frame_max);

  //
  channel->DeclareQueue(rpc_queue, false, false, false, true);

  SimpleRpcServer::ptr_t simpleServer = SimpleRpcServer::Create(channel, rpc_queue);


  // serialize the object into the buffer.
  // any classes that implements write(const char*,size_t) can be a buffer.


  int i(0);

  while(1)
  {
    auto msg = simpleServer->GetNextIncomingMessage();
    cout << msg->Body() << "\t " << i << endl;
    
    string responseMsg("1");
    msgpack::type::tuple<int, bool, std::string> src(i++, true, "example");
    std::stringstream buffer;
    msgpack::pack(buffer, src);

    // send the buffer ...
    buffer.seekg(0);

    // deserialize the buffer into msgpack::object instance.
    std::string str(buffer.str());

    simpleServer->RespondToMessage(msg, str);
  }




  return 0;
}